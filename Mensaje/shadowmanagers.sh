#!/bin/bash
declare -A cor=( [0]="\033[1;37m" [1]="\033[1;34m" [2]="\033[1;35m" [3]="\033[1;32m" [4]="\033[1;31m" [5]="\033[1;33m" [6]="\E[44;1;37m" [7]="\E[41;1;37m" )
barra="\033[0m\e[31m======================================================\033[1;37m"
SCPdir="/etc/newadm" && [[ ! -d ${SCPdir} ]] && exit 1
SCPfrm="/etc/ger-frm" && [[ ! -d ${SCPfrm} ]] && exit
SCPinst="/etc/ger-inst" && [[ ! -d ${SCPinst} ]] && exit
SCPidioma="${SCPdir}/idioma" && [[ ! -e ${SCPidioma} ]] && touch ${SCPidioma}

fun_bar () {
comando="$1"
 _=$(
$comando > /dev/null 2>&1
) & > /dev/null
pid=$!
while [[ -d /proc/$pid ]]; do
echo -ne " \033[1;33m["
   for((i=0; i<10; i++)); do
   echo -ne "\033[1;31m##"
   sleep 0.2
   done
echo -ne "\033[1;33m]"
sleep 1s
echo
tput cuu1 && tput dl1
done
echo -e " \033[1;33m[\033[1;31m####################\033[1;33m] - \033[1;32m100%\033[0m"
sleep 1s
}
mens () {
echo -e "\033[1;31m POR EL MOMENTO NO ESTA DISPONIBLE"
exit
}
shadowe_fun () {
echo -e " ${cor[7]} $(fun_trans "SHADOWSOCKS MANAGER") ${cor[6]}[ALEX_DROID9_0_MX]\033[0m"
echo -e "$barra"
while true; do
echo -e "${cor[4]} [1] > ${cor[5]}$(fun_trans "INSTALAR SHADOWSOCKS SSR")"
echo -e "${cor[4]} [2] > ${cor[5]}$(fun_trans "INSTALAR SHADOWSOCKS CLOAK")"
echo -e "${cor[4]} [3] > ${cor[5]}$(fun_trans "INSTALAR SHADOWSOCKS LIBEV")"
echo -e "${cor[4]} [4] > ${cor[5]}$(fun_trans "ACTUALIZAR ZONA HORARIO")"
echo -e "${cor[4]} [5] > ${cor[0]}$(fun_trans "VOLVER")"
echo -e "${cor[4]} [0] > ${cor[0]}$(fun_trans "SALIR")\n${barra}"
while [[ ${opx} != @(0|[1-8]) ]]; do
echo -ne "${cor[0]}$(fun_trans "Digite una Opcion"): \033[1;37m" && read opx
tput cuu1 && tput dl1
done
case $opx in
	0)
	exit;;
	1)
	wget -O /bin/ssrrmu.sh https://www.dropbox.com/s/cwumatottx4q174/ssrrmu.sh > /dev/null 2>&1; chmod +x /bin/ssrrmu.sh; ssrrmu.sh
	break;;
	2)
	wget -O /bin/shadowsocks-cloak.sh https://www.dropbox.com/s/ronzn5tcns5o7h9/shadowsocks-cloak.sh > /dev/null 2>&1; chmod +x /bin/shadowsocks-cloak.sh; shadowsocks-cloak.sh
	break;;
	3)
	wget -O /bin/shadowsocks.sh https://www.dropbox.com/s/2ia79xuj1n6se1j/shadowsocks.sh > /dev/null 2>&1; chmod +x /bin/shadowsocks.sh; shadowsocks.sh
   break;;
	4)
	wget -O /bin/hora.sh https://www.dropbox.com/s/kotj2c925piemwk/hora.sh > /dev/null 2>&1; chmod +x /bin/hora.sh; hora.sh
	break;;
    5)
	menu;;
  
esac
done
}
shadowe_fun