#/bin/bash

echo -e "\033[1;33m- - - - -> \033[01;34mScript Configurar o servidor para usar SSL/TLS Stunnel4"
echo -e "\033[1;33m #################"
echo -e "\033[1;33m- - - - -> \033[01;34mScript QUE CONFIGURA EL PUERTO SSL / TLS Stunnel4"
echo -e "\033[1;33m #################"
echo -e "\033[1;31mBY ALEX_DROID9_0_MX"
sleep 2

apt-get update -y
clear
yum update -y
apt-get install openssh-server -y
clear
apt-get install curl -y
clear
yum install openssh-server -y
clear
apt-get install openssh-client -y
clear
yum install openssh-client -y
clear
apt-get install stunnel4 -y
clear
yum install stunnel4 -y
clear
apt-get install stunnel -y
clear
yum install stunnel -y
clear

echo -e "\033[1;31mC A P T U R A N D O  I P"
ip=$(curl https://api.ipify.org/)
echo $ip
clear

echo -e "\033[1;33m #################"
echo -e "\033[1;31mPRECIONE ENTER EN TODAS LAS CONFIGURACIONES"
echo -e "\033[1;33m ###############################"
sleep 1

echo -e "\033[1;33m ######################################"
echo -e "\033[1;31mG E R A N D O  C E R T I F I C A D O"
echo -e "\033[1;33m ######################################"
sleep 1
openssl genrsa 2048 > stunnel.key
openssl req -new -key stunnel.key -x509 -days 1000 -out stunnel.crt

echo -e "\033[1;33m ######################################"
echo -e "\033[1;31mC R E A N D O  U N A  N U E V A  C O N F I G U R A C I O N"
echo -e "\033[1;33m ######################################"
sleep 2
rm /etc/stunnel/stunnel.conf
clear
rm /etc/default/stunnel4
clear
cat stunnel.crt stunnel.key > stunnel.pem 
mv stunnel.pem /etc/stunnel/
clear
echo -e "\E[1;41mD I G I T E  SU  P U E R T O  Q U E  D E S E A  DEJAR\033[0m"
echo -e "\033[1;33mPUERTO  S S L"
read -p ": " port
clear

echo -e "\033[1;33m ######################################"
echo -e "\033[1;31mC O N F I G U R A N D O  STUNNEL.CONF"
echo -e "\033[1;33m ######################################"
sleep 1


echo "cert = /etc/stunnel/stunnel.pem " >> /etc/stunnel/stunnel.conf
echo "client = no " >> /etc/stunnel/stunnel.conf
echo "sslVersion = ALL " >> /etc/stunnel/stunnel.conf
echo "socket = a:SO_REUSEADDR=1 " >> /etc/stunnel/stunnel.conf
echo "socket = l:TCP_NODELAY=1 " >> /etc/stunnel/stunnel.conf
echo "socket = r:TCP_NODELAY=1 " >> /etc/stunnel/stunnel.conf
echo "" >> /etc/stunnel/stunnel.conf
echo "[ssh] " >> /etc/stunnel/stunnel.conf
echo "connect = 127.0.0.1:22 " >> /etc/stunnel/stunnel.conf
echo "accept = $port " >> /etc/stunnel/stunnel.conf


echo -e "\033[1;33m ######################################"
echo -e "\033[1;31mC O N F I G U R A N D O  STUNNEL4"
echo -e "\033[1;33m ######################################"
sleep 1

echo "ENABLED=1 " >> /etc/default/stunnel4
echo "FILES="/etc/stunnel/*.conf" " >> /etc/default/stunnel4
echo "OPTIONS="" " >> /etc/default/stunnel4
echo "PPP_RESTART=0" >> /etc/default/stunnel4

echo -e "\033[1;33m ######################################"
echo -e "\033[1;31mI N I C I A N D O  O  STUNNEL4"
echo -e "\033[1;33m ######################################"
sleep 1

echo -e "\033[1;33m ######################################"
echo -e "\033[1;31m ###C O R R E G I R  E R  R O R E S  ESTO ES  N O R M A L AGUARDE...####"
echo -e "\E[1;33mNO TE DESESPERES EN UN MOMENTO ESTARÁ LISTO\033[0m" 
echo -e "\033[1;33m ######################################"
sleep 1
service ssh start 1>/dev/null 2 /dev/null
/etc/init.d/ssh start 1>/dev/null 2 /dev/null
service sshd start 1>/dev/null 2 /dev/null
/etc/init.d/sshd start 1>/dev/null 2 /dev/null
service sshd restart 1>/dev/null 2 /dev/null
/etc/init.d/sshd restart 1>/dev/null 2 /dev/null
service ssh restart 1>/dev/null 2 /dev/null
/etc/init.d/ssh restart 1>/dev/null 2 /dev/null
service stunnel4 start 1>/dev/null 2 /dev/null
/etc/init.d/stunnel4 start 1>/dev/null 2 /dev/null
service stunnel4 restart
/etc/init.d/stunnel4 restart 1>/dev/null 2 /dev/null
systemctl start stunnel4 1>/dev/null 2 /dev/null
systemctl restart stunnel 1>/dev/null 2 /dev/null
clear

sleep 2
echo -e "\033[1;33m ###########REINICIADO...###########"
clear

echo -e "\033[1;33m ######################################"
echo -e "\033[1;31mC O N F I G U R A D O  C O N  E X I T O "
echo -e "\033[1;33m ######################################"
echo -e "\033[1;33m- - - - -> \033[01;34mSU IP HOST:\033[0m $ip"
echo -e "\033[1;33m- - - - -> \033[01;34mSU PUERTO SSL:\033[0m $port"
sleep 1
echo -e "\033[1;31mEL SERVIDOR SERÁ REINICIADO MODO SUDO REBOOT"
sleep 2
echo -e "\033[1;33m- - ->>C A N A L \033[01;34mhttps://t.me/joinchat/JBetO1FJ7jhwAMC9FyJ4mA"
echo -e "\033[1;33m- - ->>CREADO POR \033[01;34mBY @ALEX_DROID9_0_MX"
sleep 1
sudo reboot